package com.ljadanowski.webpagedownloader.repository;

import com.ljadanowski.webpagedownloader.entity.WebPage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WebPageRepository extends CrudRepository<WebPage, Integer> {
    List<WebPage> findByUrlIn(String[] url);

    WebPage findByUrl(String url);

    @Query("SELECT w FROM WebPage w where content like %:searchText%")
    List<WebPage> searchTextInContent(String searchText);
}
