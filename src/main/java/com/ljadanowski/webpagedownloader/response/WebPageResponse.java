package com.ljadanowski.webpagedownloader.response;

import com.ljadanowski.webpagedownloader.entity.WebPage;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Tolerate;

import java.util.List;
import java.util.stream.Collectors;

@Builder
@Getter
@EqualsAndHashCode
public class WebPageResponse {
    private int id;
    private String url;
    private String content;

    @Tolerate
    public WebPageResponse() {
    }

    public static List<WebPageResponse> from(List<WebPage> webPages) {
        return webPages
                .stream()
                .map(WebPageResponse::create)
                .collect(Collectors.toList());
    }

    public static WebPageResponse from(WebPage webPage) {
        return create(webPage);
    }

    private static WebPageResponse create(WebPage webPage) {
        return WebPageResponse.builder()
                .id(webPage.getId())
                .url(webPage.getUrl())
                .content(webPage.getContent())
                .build();
    }
}