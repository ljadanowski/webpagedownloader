package com.ljadanowski.webpagedownloader.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@ToString
public class WebPageRequest {
    @NotEmpty
    @URL
    private String url;
}
