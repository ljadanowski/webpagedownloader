package com.ljadanowski.webpagedownloader.service;

import com.ljadanowski.webpagedownloader.annotation.ExecutionTime;
import com.ljadanowski.webpagedownloader.component.PageConverter;
import com.ljadanowski.webpagedownloader.entity.WebPage;
import com.ljadanowski.webpagedownloader.repository.WebPageRepository;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WebPageDownloaderService {
    private final WebPageRepository webPageRepository;
    private final PageConverter pageConverter;

    public WebPageDownloaderService(WebPageRepository webPageRepository, PageConverter pageConverter) {
        this.webPageRepository = webPageRepository;
        this.pageConverter = pageConverter;
    }

    @Async
    @ExecutionTime
    public void save(String url) throws Exception {
        final String pageContent = pageConverter.convert(url);
        saveInDatabase(url, pageContent);
    }

    public List<WebPage> searchTextInContent(String text) {
        return webPageRepository.searchTextInContent(text);
    }

    public List<WebPage> findByUrls(String[] urls) {
        return webPageRepository.findByUrlIn(urls);
    }

    private void saveInDatabase(String url, String pageContent) {
        WebPage webPage = new WebPage();
        webPage.setUrl(url);
        webPage.setContent(pageContent);
        webPageRepository.save(webPage);
    }
}
