package com.ljadanowski.webpagedownloader.service;

import com.ljadanowski.webpagedownloader.config.RabbitConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UrlReceiver {
    private final WebPageDownloaderService webPageDownloaderService;

    public UrlReceiver(WebPageDownloaderService webPageDownloaderService) {
        this.webPageDownloaderService = webPageDownloaderService;
    }

    @RabbitListener(queues = RabbitConfig.queueName)
    public void receive(String url) throws Exception {
        log.info("Received and saving url: {}", url);
        webPageDownloaderService.save(url);
    }
}
