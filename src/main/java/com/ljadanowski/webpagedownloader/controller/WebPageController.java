package com.ljadanowski.webpagedownloader.controller;

import com.ljadanowski.webpagedownloader.config.RabbitConfig;
import com.ljadanowski.webpagedownloader.request.WebPageRequest;
import com.ljadanowski.webpagedownloader.response.WebPageResponse;
import com.ljadanowski.webpagedownloader.service.WebPageDownloaderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/web-page")
@Slf4j
public class WebPageController {
    private final WebPageDownloaderService webPageDownloaderService;
    private final RabbitTemplate rabbitTemplate;

    public WebPageController(WebPageDownloaderService webPageDownloaderService, RabbitTemplate rabbitTemplate) {
        this.webPageDownloaderService = webPageDownloaderService;
        this.rabbitTemplate = rabbitTemplate;
    }

    @GetMapping
    public List<WebPageResponse> findByUrls(@RequestParam String[] urls) {
        return WebPageResponse.from(webPageDownloaderService.findByUrls(urls));
    }

    @GetMapping("/search")
    public List<WebPageResponse> searchTextInContent(@RequestParam String text) {
        return WebPageResponse.from(webPageDownloaderService.searchTextInContent(text));
    }

    @PostMapping
    public void send(@RequestBody @Valid WebPageRequest webPageRequest) {
        log.info("Sending url: {}", webPageRequest.getUrl());
        rabbitTemplate.convertAndSend(RabbitConfig.queueName, webPageRequest.getUrl());
    }
}
