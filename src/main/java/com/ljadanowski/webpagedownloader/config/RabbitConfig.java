package com.ljadanowski.webpagedownloader.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    public final static String queueName = "urls";

    @Bean
    public Queue queue() {
        return new Queue(queueName, false);
    }
}
