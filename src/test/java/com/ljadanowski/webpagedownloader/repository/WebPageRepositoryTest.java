package com.ljadanowski.webpagedownloader.repository;

import com.ljadanowski.webpagedownloader.entity.WebPage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class WebPageRepositoryTest {
    private final WebPageRepository webPageRepository;

    @Autowired
    public WebPageRepositoryTest(WebPageRepository webPageRepository) {
        this.webPageRepository = webPageRepository;
    }

    @Test
    public void findByLinksWhenTwoLinksAreExisted() {
        final WebPage webPage = new WebPage();
        webPage.setUrl("url 1");
        webPage.setContent("content 1");

        final WebPage webPage2 = new WebPage();
        webPage2.setUrl("url 2");
        webPage2.setContent("content 2");

        webPageRepository.save(webPage);
        webPageRepository.save(webPage2);

        final List<WebPage> webPages = webPageRepository.findByUrlIn(new String[] {"url 1", "url 2"} );
        List<String> urlsFromDatabase = webPages.stream().map(WebPage::getUrl).collect(Collectors.toList());
        assertThat(urlsFromDatabase).containsExactlyInAnyOrder("url 1", "url 2");
    }

    @Test
    public void findByLinksWhenOneLinkIsExisted() {
        // GIVEN
        final WebPage webPage = new WebPage();
        webPage.setUrl("url 1");
        webPage.setContent("content 1");

        final WebPage webPage2 = new WebPage();
        webPage2.setUrl("url 2");
        webPage2.setContent("content 2");

        webPageRepository.save(webPage);
        webPageRepository.save(webPage2);

        // WHEN
        final List<WebPage> webPages = webPageRepository.findByUrlIn(new String[] {"url 2"} );

        // THEN
        assertThat(webPages).hasSize(1);
        assertThat(webPages.get(0).getUrl()).isEqualTo("url 2");
    }

    @Test
    public void findByLinksWhenLinkNotExisted() {
        // GIVEN
        final WebPage webPage = new WebPage();
        webPage.setUrl("url 1");
        webPage.setContent("content 1");

        final WebPage webPage2 = new WebPage();
        webPage2.setUrl("url 2");
        webPage2.setContent("content 2");

        webPageRepository.save(webPage);
        webPageRepository.save(webPage2);

        // WHEN
        final List<WebPage> webPages = webPageRepository.findByUrlIn(new String[] {"other link"} );

        // THEN
        assertThat(webPages).isEmpty();
    }

    @Test
    public void findByTextWhenTextIsContainingInContent() {
        // GIVEN
        final WebPage webPage = new WebPage();
        webPage.setUrl("url 1");
        webPage.setContent("content 1");

        final WebPage webPage2 = new WebPage();
        webPage2.setUrl("url 2");
        webPage2.setContent("content 2");

        webPageRepository.save(webPage);
        webPageRepository.save(webPage2);

        // WHEN
        final List<WebPage> webPages = webPageRepository.searchTextInContent("tent");

        // THEN
        assertThat(webPages).hasSize(2);
        assertThat(webPages.get(0).getContent()).contains("tent");
        assertThat(webPages.get(1).getContent()).contains("tent");
    }

    @Test
    public void findByTextWhenTextIsNotContainingInContent() {
        // GIVEN
        final WebPage webPage = new WebPage();
        webPage.setUrl("url 1");
        webPage.setContent("content 1");

        final WebPage webPage2 = new WebPage();
        webPage2.setUrl("url 2");
        webPage2.setContent("content 2");

        webPageRepository.save(webPage);
        webPageRepository.save(webPage2);

        // WHEN
        final List<WebPage> webPages = webPageRepository.searchTextInContent("other test");

        // THEN
        assertThat(webPages).isEmpty();
    }

    @Test
    public void findByUrlGivenNotExistingUrl() {
        // GIVEN
        final WebPage webPage = new WebPage();
        webPage.setUrl("url 1");
        webPage.setContent("content 1");

        webPageRepository.save(webPage);

        // WHEN
        final WebPage webPages = webPageRepository.findByUrl("other url");

        // THEN
        assertThat(webPages).isNull();
    }

    @Test
    public void findByUrlGivenExistingUrl() {
        // GIVEN
        final WebPage webPage = new WebPage();
        webPage.setUrl("url 1");
        webPage.setContent("content 1");

        webPageRepository.save(webPage);

        // WHEN
        final WebPage webPages = webPageRepository.findByUrl("url 1");

        // THEN
        assertThat(webPages).isNotNull();
        assertThat(webPages.getUrl()).isEqualTo("url 1");
    }
}