package com.ljadanowski.webpagedownloader.service;

import com.ljadanowski.webpagedownloader.component.PageConverter;
import com.ljadanowski.webpagedownloader.entity.WebPage;
import com.ljadanowski.webpagedownloader.repository.WebPageRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WebPageDownloaderServiceTest {
    @Mock
    private PageConverter pageConverter;

    @Mock
    private WebPageRepository webPageRepository;

    @InjectMocks
    private WebPageDownloaderService webPageDownloaderService;

    @Test
    public void saveGivenCorrectUrl() throws Exception {
        // GIVEN
        final String url = "url";

        final WebPage webPage = new WebPage();
        webPage.setUrl(url);
        webPage.setContent("content");

        when(pageConverter.convert(url)).thenReturn("content");
        when(webPageRepository.save(webPage)).thenReturn(webPage);

        // WHEN
        webPageDownloaderService.save(url);

        // THEN
        verify(pageConverter).convert(url);
        verify(webPageRepository).save(webPage);
    }

    @Test
    public void saveGivenIncorrectUrl() throws Exception {
        // GIVEN
        final String url = "incorrect url";

        when(pageConverter.convert(url)).thenThrow(IOException.class);

        // WHEN
        assertThrows(IOException.class, () -> webPageDownloaderService.save(url));

        // THEN
        verify(pageConverter).convert(url);
        verify(webPageRepository, never()).save(any());
    }
}