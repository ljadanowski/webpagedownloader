package com.ljadanowski.webpagedownloader.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;

import java.time.Duration;

@TestConfiguration(proxyBeanMethods = false)
public abstract class TestConfig {
    private final int TIMEOUT_IN_SECONDS = 5;

    @Bean
    protected RestTemplateBuilder restTemplateBuilder() {
        return new RestTemplateBuilder()
                .setConnectTimeout(Duration.ofSeconds(TIMEOUT_IN_SECONDS))
                .setReadTimeout(Duration.ofSeconds(TIMEOUT_IN_SECONDS));
    }
}
