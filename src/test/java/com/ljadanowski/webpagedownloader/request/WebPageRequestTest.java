package com.ljadanowski.webpagedownloader.request;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class WebPageRequestTest {
    private static Validator validator;

    @BeforeAll
    public static void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void validateWhenUrlIsNull() {
        // GIVEN
        final WebPageRequest webPageRequest = new WebPageRequest();

        // WHEN
        final Set<ConstraintViolation<WebPageRequest>> errors = validator.validate(webPageRequest);

        // THEN
        final List<String> messages = convertToMessageTemplatesList(errors);

        assertThat(messages).isNotEmpty();
        assertThat(messages.get(0)).isEqualTo("{javax.validation.constraints.NotEmpty.message}");
    }

    @Test
    public void validateWhenUrlIsEmpty() {
        // GIVEN
        final WebPageRequest webPageRequest = new WebPageRequest();
        webPageRequest.setUrl("");

        // WHEN
        final Set<ConstraintViolation<WebPageRequest>> errors = validator.validate(webPageRequest);

        // THEN
        final List<String> messages = convertToMessageTemplatesList(errors);

        assertThat(messages).isNotEmpty();
        assertThat(messages.get(0)).isEqualTo("{javax.validation.constraints.NotEmpty.message}");
    }

    @Test
    public void validateWhenUrlIsInvalid() {
        // GIVEN
        final WebPageRequest webPageRequest = new WebPageRequest();
        webPageRequest.setUrl("Invalid URL");

        // WHEN
        final Set<ConstraintViolation<WebPageRequest>> errors = validator.validate(webPageRequest);

        // THEN
        final List<String> messages = convertToMessageTemplatesList(errors);

        assertThat(messages).isNotEmpty();
        assertThat(messages.get(0)).isEqualTo("{org.hibernate.validator.constraints.URL.message}");
    }

    @Test
    public void validateWhenUrlIsValid() {
        // GIVEN
        final WebPageRequest webPageRequest = new WebPageRequest();
        webPageRequest.setUrl("http://google.pl");

        // WHEN
        final Set<ConstraintViolation<WebPageRequest>> errors = validator.validate(webPageRequest);

        // THEN
        assertThat(errors).isEmpty();
    }

    @Test
    public void validateWhenUrlIsValid2() {
        // GIVEN
        final WebPageRequest webPageRequest = new WebPageRequest();
        webPageRequest.setUrl("https://google.pl");

        // WHEN
        final Set<ConstraintViolation<WebPageRequest>> errors = validator.validate(webPageRequest);

        // THEN
        assertThat(errors).isEmpty();
    }

    private List<String> convertToMessageTemplatesList(Set<ConstraintViolation<WebPageRequest>> errors) {
        return errors.stream()
                .map(ConstraintViolation::getMessageTemplate)
                .collect(Collectors.toList());
    }
}
