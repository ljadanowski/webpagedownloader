package com.ljadanowski.webpagedownloader.response;

import com.ljadanowski.webpagedownloader.entity.WebPage;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class WebPageResponseTest {

    @Test
    public void fromGivenListOfWebPages() {
        // GIVEN
        final WebPage firstWebPage = new WebPage();
        firstWebPage.setId(1);
        firstWebPage.setUrl("Url 1");
        firstWebPage.setContent("Content 1");

        final WebPage secondWebPage = new WebPage();
        secondWebPage.setId(2);
        secondWebPage.setUrl("Url 2");
        secondWebPage.setContent("Content 2");

        final List<WebPage> webPages = Arrays.asList(firstWebPage, secondWebPage);

        // WHEN
        final List<WebPageResponse> webPagesResponse = WebPageResponse.from(webPages);

        // THEN
        assertNotNull(webPagesResponse);
        assertEquals(2, webPagesResponse.size());

        final WebPageResponse firstWebPageResponse = webPagesResponse.get(0);
        final WebPageResponse secondWebPageResponse = webPagesResponse.get(1);

        assertEquals(1, firstWebPageResponse.getId());
        assertEquals("Url 1", firstWebPageResponse.getUrl());
        assertEquals("Content 1", firstWebPageResponse.getContent());

        assertEquals(2, secondWebPageResponse.getId());
        assertEquals("Url 2", secondWebPageResponse.getUrl());
        assertEquals("Content 2", secondWebPageResponse.getContent());
    }

    @Test
    public void fromGivenWebContent() {
        // GIVEN
        final WebPage webPage = new WebPage();
        webPage.setId(100);
        webPage.setUrl("http://xx.pl");
        webPage.setContent("<html> </html>");

        // WHEN
        final WebPageResponse webPageResponse = WebPageResponse.from(webPage);

        // THEN
        assertEquals(100, webPageResponse.getId());
        assertEquals("http://xx.pl", webPageResponse.getUrl());
        assertEquals("<html> </html>", webPageResponse.getContent());
    }
}