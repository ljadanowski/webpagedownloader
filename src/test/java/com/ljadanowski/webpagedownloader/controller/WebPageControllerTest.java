package com.ljadanowski.webpagedownloader.controller;

import com.ljadanowski.webpagedownloader.config.TestConfig;
import com.ljadanowski.webpagedownloader.entity.WebPage;
import com.ljadanowski.webpagedownloader.repository.WebPageRepository;
import com.ljadanowski.webpagedownloader.request.WebPageRequest;
import com.ljadanowski.webpagedownloader.response.WebPageResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WebPageControllerTest extends TestConfig {

    private static final int TIMEOUT = 10;
    private final TestRestTemplate testRestTemplate;
    private final WebPageRepository webPageRepository;

    @Autowired
    public WebPageControllerTest(TestRestTemplate testRestTemplate, WebPageRepository webPageRepository) {
        this.testRestTemplate = testRestTemplate;
        this.webPageRepository = webPageRepository;
    }

    @BeforeEach
    public void clearDatabase() {
        webPageRepository.deleteAll();
    }

    @Test
    void findByUrlsWhenUrlsAreExisted() {
        // GIVEN
        final WebPage webContent = new WebPage();
        webContent.setUrl("111");
        webContent.setContent("aaa");

        final WebPage webContent2 = new WebPage();
        webContent2.setUrl("222");
        webContent2.setContent("bbb");

        final WebPage webContent3 = new WebPage();
        webContent3.setUrl("333");
        webContent3.setContent("ccc");

        webPageRepository.saveAll(Arrays.asList(webContent, webContent2, webContent3));

        // WHEN
        ResponseEntity<List<WebPageResponse>> responseEntity = testRestTemplate.exchange(
                "/web-page?urls=111,333",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<>() {}
        );

        //THEN
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        List<WebPageResponse> webPageResponses = responseEntity.getBody();
        assertThat(webPageResponses).hasSize(2);

        WebPageResponse firstWebPageResponse = webPageResponses.get(0);
        assertThat(firstWebPageResponse.getUrl()).isEqualTo(webContent.getUrl());
        assertThat(firstWebPageResponse.getContent()).isEqualTo(webContent.getContent());

        WebPageResponse secondWebPageResponse = webPageResponses.get(1);
        assertThat(secondWebPageResponse.getUrl()).isEqualTo(webContent3.getUrl());
        assertThat(secondWebPageResponse.getContent()).isEqualTo(webContent3.getContent());
    }

    @Test
    void findByUrlsWhenUrlsAreNotExisted() {
        // GIVEN
        final WebPage webContent = new WebPage();
        webContent.setUrl("111");
        webContent.setContent("aaa");

        final WebPage webContent2 = new WebPage();
        webContent2.setUrl("222");
        webContent2.setContent("bbb");

        final WebPage webContent3 = new WebPage();
        webContent3.setUrl("333");
        webContent3.setContent("ccc");

        webPageRepository.saveAll(Arrays.asList(webContent, webContent2, webContent3));

        // WHEN
        ResponseEntity<List<WebPageResponse>> responseEntity = testRestTemplate.exchange(
                "/web-page?urls=444,555",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<>() {}
        );

        //THEN
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isEmpty();
    }

    @Test
    void findByUrlsWhenUrlIsInvalid() {
        // GIVEN
        final WebPage webContent = new WebPage();
        webContent.setUrl("111");
        webContent.setContent("aaa");

        final WebPage webContent2 = new WebPage();
        webContent2.setUrl("222");
        webContent2.setContent("bbb");

        final WebPage webContent3 = new WebPage();
        webContent3.setUrl("333");
        webContent3.setContent("ccc");

        webPageRepository.saveAll(Arrays.asList(webContent, webContent2, webContent3));

        // WHEN
        ResponseEntity<List<WebPageResponse>> responseEntity = testRestTemplate.exchange(
                "/web-page?urls=invalid",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<>() {}
        );

        //THEN
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).isEmpty();
    }

    @Test
    public void searchTextInContent() {
        // GIVEN
        final WebPage webContent = new WebPage();
        webContent.setUrl("http://wp.pl");
        webContent.setContent("Lorem ipsum dolor sit amet, adipiscing elit. Aenean dapibus vel velit ut convallis.");

        final WebPage webContent2 = new WebPage();
        webContent2.setUrl("http://onet.pl");
        webContent2.setContent("Praesent facilisis rutrum sem consectetur, quis commodo lectus mattis a. Proin. ");

        final WebPage webContent3 = new WebPage();
        webContent3.setUrl("http://gogole.pl");
        webContent3.setContent("Praesent ullamcorper luctus consectetur dignissim. Mauris nunc nulla, efficitur dapibus purus.");

        webPageRepository.saveAll(Arrays.asList(webContent, webContent2, webContent3));

        // WHEN
        ResponseEntity<List<WebPageResponse>> responseEntity = testRestTemplate.exchange(
                "/web-page/search?text=consectetur",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<>() {}
        );

        //THEN
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(responseEntity.getBody()).hasSize(2);

        final List<WebPageResponse> webPageResponses = responseEntity.getBody();
        final List<String> urls = webPageResponses.stream().map(WebPageResponse::getUrl).collect(Collectors.toList());

        assertThat(urls).containsExactlyInAnyOrder(webContent2.getUrl(), webContent3.getUrl());
    }

    @Test
    public void sendGivenValidUrl() {
        // GIVEN
        final WebPageRequest webPageRequest = new WebPageRequest();
        webPageRequest.setUrl("http://wp.pl");

        // WHEN
        ResponseEntity<Void> responseEntity = testRestTemplate.exchange(
                "/web-page",
                HttpMethod.POST,
                new HttpEntity<>(webPageRequest),
                Void.class
        );

        //THEN
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        await().atMost(TIMEOUT, TimeUnit.SECONDS)
                .untilAsserted(() -> assertThat(webPageRepository.findByUrl("http://wp.pl")).isNotNull());
    }

    @Test
    public void sendGivenInvalidUrl() {
        // GIVEN
        final WebPageRequest webPageRequest = new WebPageRequest();
        webPageRequest.setUrl("Invalid url");

        // WHEN
        ResponseEntity<Void> responseEntity = testRestTemplate.exchange(
                "/web-page",
                HttpMethod.POST,
                new HttpEntity<>(webPageRequest),
                Void.class
        );

        //THEN
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }
}