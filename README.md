### Steps ###

* Go to main application directory
* Run command: docker-compose up -d
* Run application or tests
* Send example request:

```
curl --location --request POST 'localhost:8080/web-page' \
--header 'Content-Type: application/json' \
--data-raw '{
    "url":"http://www.wp.pl"
}'
```

#### Rabbit console 
```
localhost:15672

login: guest
password: guest
```

#### Mysql database 
```
jdbc:mysql://localhost:3306/mydb

login: user
password: password
```
